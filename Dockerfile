FROM debian:9

RUN apt-get update && \
    apt-get install -y \
    git \
    qemu-user-static \
    binfmt-support \
    make \
    cmake \
    build-essential \
    automake \
    autoconf \
    libtool \
    wget \
    xxd \
    desktop-file-utils \
    pkg-config \
    libglib2.0-dev \
    libcairo2-dev \
    libfuse-dev \
    zsync
	
RUN git clone --depth 1 --branch 12 --recursive https://github.com/AppImage/AppImageKit
RUN cd AppImageKit/ && bash -ex build.sh

# Clean up to reduce image size
RUN apt-get clean && \
    rm -rf \
        /var/lib/apt/lists/* \
        /tmp/* \
        /var/tmp/*
